import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from './core/home/home.component';
import { ForbiddenComponent } from './core/errors/forbidden.component';
import { NotFoundComponent } from './core/errors/not-found.component';
import { ContactComponent } from './core/contact/contact.component';

const appRoutes: Routes = [
    // Home Route
    { path: 'home', component: HomeComponent},
    { path: 'contacts', component: ContactComponent},

    // Errors Routes
    { path: 'error/404', component: NotFoundComponent },
    { path: 'error/403', component: ForbiddenComponent },

    // Lazy loads Routes
    { path: 'user', loadChildren: 'app/modules/user/user.module#UserModule' },
    { path: 'encyclopedia', loadChildren: 'app/modules/encyclopedia/encyclopedia.module#EncyclopediaModule' },
    { path: 'post', loadChildren: 'app/modules/post/post.module#PostModule' },

    // Index Route
    { path: '', component: HomeComponent },

    // Fallback Route
    { path: '**', component:  NotFoundComponent}
];

export const appRouting = RouterModule.forRoot(appRoutes);
