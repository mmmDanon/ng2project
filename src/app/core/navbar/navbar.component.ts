import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../shared/security/auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: 'navbar.component.html',
  styles: [],
})
export class NavbarComponent implements OnInit {

  public userInfo: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.userInfo = this.authService.userInfo;
  }

  logout() {
    this.authService.logout();
  }



}
