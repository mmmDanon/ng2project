import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../shared/security/auth.service";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  public user: any;
  public authenticated: boolean;


  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.userInfo;
    this.authenticated = this.authService.isLogged;
  }

}
