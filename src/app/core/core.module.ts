import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { ForbiddenComponent } from './errors/forbidden.component';
import { NotFoundComponent } from './errors/not-found.component';
import {NavbarComponent} from "./navbar/navbar.component";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import { SidebarComponent } from './sidebar/sidebar.component';
import { FooterComponent } from './footer/footer.component';
import { ContactComponent } from './contact/contact.component';
import {ReactiveFormsModule} from "@angular/forms";
import {DropdownModule} from "ngx-dropdown";

@NgModule({
  declarations: [HomeComponent, ForbiddenComponent, NotFoundComponent, NavbarComponent, SidebarComponent, FooterComponent, ContactComponent],
  exports: [NavbarComponent, SidebarComponent, FooterComponent],
  imports: [BrowserModule,RouterModule, ReactiveFormsModule, DropdownModule]
})
export class CoreModule { }
