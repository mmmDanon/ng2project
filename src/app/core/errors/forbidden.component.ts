import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forbidden',
  template: `
    <h1>Ошибка 404</h1>
    <p>
      Доступ к этой странице запрещен!
    </p>
  `,
  styles: []
})
export class ForbiddenComponent implements OnInit {

  constructor() { }

  ngOnInit() {

  }

}
