import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `
    <h1>Ошибка 404</h1>
    <p>Такой страницы не существует. Вернуться на <a [routerLink]="['/home']">главную</a></p>
  `,
  styles: []
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
