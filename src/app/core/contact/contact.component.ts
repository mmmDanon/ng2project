import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import 'rxjs/add/operator/map';
import {AuthHttp} from "../../shared/security/AuthHttp";
import {ToastsManager} from "ng2-toastr";
import {WebConfig} from "../../shared/web.config";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  private baseUrl: string;
  private url: string = '/main/contact/index';

  contactForm: FormGroup;
  public pending = false;
  private formErrors = {
    email: '',
    text: '',
    type: ''
  };

  private errorMessages = {
    email: {
      required: 'Заполните это поле',
      pattern: 'Некорректный email'
    },
    text: {
      required: 'Заполните это поле',
      minlength: 'Сообщение должно быть не менее 25 символов'
    },
    type: {
      required: 'Заполните это поле',
    }
  };

  constructor(private fb: FormBuilder, private http: AuthHttp,public toastr: ToastsManager, vcr: ViewContainerRef, private webConfig: WebConfig) {
    this.toastr.setRootViewContainerRef(vcr);
    this.baseUrl = this.webConfig.baseUrl;
  }

  ngOnInit() {
    this.contactForm = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)
      ])],
      text: ['', Validators.compose([
        Validators.required,
        Validators.minLength(25)
      ])],
      type: ['0', Validators.required]
    });
    this.contactForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.contactForm) { return; }
    const form = this.contactForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '. ';
        }
      }
    }
  }

  send() {
    if (!this.contactForm.valid) {
      return;
    }
    console.log(this.contactForm.value);
    this.http.post(this.baseUrl + this.url, this.contactForm.value).map(res => res.json())
      .subscribe(res => {
        this.contactForm.reset();
        this.toastr.success(res);
      }, error => {
        error = error.json();
        this.toastr.success(error.mess);
      });
  }

}
