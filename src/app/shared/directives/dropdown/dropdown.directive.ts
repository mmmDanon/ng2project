import {Directive, HostListener, HostBinding, ElementRef} from '@angular/core';

@Directive({
  selector: '[adDropdown]'
})
export class DropdownDirective {

  private isOpen: boolean = false;

  constructor(private _eref: ElementRef) {

  }

  @HostBinding('class.open') get opened() {
    return this.isOpen;
  }
  // @HostListener('mouseenter') open() {
  //   this.isOpen = true;
  // }
  @HostListener('mouseup') open() {
    this.isOpen = !this.isOpen;
  }
  // @HostListener('mouseleave') close() {
  //   this.isOpen = false;
  // }

}
