import {Injectable} from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import {AuthService} from "./auth.service";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable()
export class AuthHttp {

  constructor(private http: Http, private authService: AuthService, private router: Router) {}

  get(url, headers?: Headers) {
    return this.intercept(this.http.get(url, headers));
  }

  post(url, data, headers?: Headers) {
    return this.intercept(this.http.post(url, data, headers));
  }

  intercept(observable: Observable<Response>): Observable<Response> {
    return observable.catch((err) => {
      if (err.status === 401) {
        this.authService.logout();
        this.router.navigate(['/user']);
        return Observable.empty();
      } else if (err.status === 403) {
        this.authService.logout();
        this.router.navigate(['/error/403']);
        return Observable.empty();
      } else if (err.status === 404) {
        this.router.navigate(['/error/404']);
        return Observable.empty();
      } else {
        return Observable.throw(err);
      }
    });
  }


}
