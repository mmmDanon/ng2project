import { Injectable} from '@angular/core';
import { UserInterface} from './user.interface';
import {Http} from "@angular/http";
import {WebConfig} from "../web.config";

@Injectable()
export class AuthService{

  private baseUrl: string;
  private urlLogout = '/user/default/logout';
  public userInfo = {
    isLogged: false
  };

  constructor(private http: Http, private webConfig: WebConfig) {
    this.userInfo.isLogged = this.isLogged;
    this.baseUrl = webConfig.baseUrl;
  }

  authenticate(user: UserInterface) {
    localStorage.setItem('userIdentity', JSON.stringify(user));
    this.userInfo.isLogged = true;
  }

  logout() {
    if(this.isLogged) {
      localStorage.removeItem('userIdentity');
      this.userInfo.isLogged = false;
    }
    this.http.post(this.baseUrl + this.urlLogout, '').subscribe(res => {
      console.log('Сессия закончилась, авторизуйтесь');
    });
  }

  get user(): UserInterface | boolean {
    const userStringify = localStorage.getItem('userIdentity');
    if (!userStringify) { return false; }
    return JSON.parse(userStringify);
  }

  get token(): string | null {
    const user = this.user;
    if (!user) { return null; }
    return user['auth_key'];
  }

  get isLogged(): boolean {

    if (localStorage.getItem('userIdentity')) {
      return true;
    }
    return false;
  }



}
