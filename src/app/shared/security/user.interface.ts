export interface UserInterface {
  email: string;
  password: string;
  repeatPassword?: string;
  role: string;
  status: number;
  auth_key?: string;
}
