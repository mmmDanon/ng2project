import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Post} from "../post.interface";
import {PostService} from "../post.service";
import {ActivatedRoute, Router} from "@angular/router";

import 'rxjs/add/operator/map';
import {Tag} from "../tag.interface";
import {AuthService} from "../../../shared/security/auth.service";
import {ToastsManager} from "ng2-toastr";


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styles: [
    '.post-vote a {cursor: pointer;}'
  ]
})
export class PostDetailComponent implements OnInit {

  private userInfo: any;
  public commentForm = {
    text: '',
    user_id: null,
    post_id: null
  };

  public postDetail: Post;
  public tags: Tag[];
  public postId: number;
  public postOwnerName: string;
  public comments: any[];

  constructor(private postService: PostService,
              public toastr: ToastsManager,
              vcr: ViewContainerRef,
              private aRouter: ActivatedRoute,
              private router: Router,
              private authService: AuthService) {
    this.toastr.setRootViewContainerRef(vcr);
    aRouter.params.subscribe(params => {
      this.postId = params['id'];
      if (+this.postId > 0) {
        return true;
      } else {
        this.router.navigate(['/error/404']);
      }
    });
  }

  ngOnInit() {
    this.userInfo = this.authService.userInfo;
    this.postService.getPostDetail(this.postId).map(res => res.json())
      .subscribe(res => {
        this.postDetail = res['post'];
        this.postOwnerName = res['username'];
        this.tags = res['tegs'];
        this.tags = res['tegs'];
        this.comments = res['comments'];
      }, error => {
        this.router.navigate(['/error/404']);
      });
  }

  addComment() {
    if (!this.userInfo.isLogged) {
      this.showError('Оставлять комментарии могут только авторизованные пользователи');
      return;
    }
    if (!this.commentForm.text) { return; }
    this.commentForm.post_id = this.postDetail.id;
    this.commentForm.user_id = this.authService.user['id'];
    this.postService.saveComment(this.commentForm).map(res => res.json())
      .subscribe(res => {
        if (!this.comments) { this.comments = [] }
        this.comments.splice(0,0,{
          comment: {
            text: res.text,
            created_at: res.created_at,
            rating: 0
          },
          username: this.authService.user['username']
        });
        this.resetComment();
      }, error => {
        error = error.json();
        this.showError(error.message);
      });
  }
  showError(error) {
    this.toastr.error(error);
  }
  resetComment() {
    this.commentForm = {
      text: '',
      user_id: null,
      post_id: null
    };
  }
  postVote(vote: number) {
    const postId = this.postId;
    const userId = this.authService.user['id'];
    this.postService.postVote(vote, postId, userId).map(res => res.json())
      .subscribe(res => {
        this.postDetail.rating = res;
      }, error => {
        error = error.json();
        this.showError(error.message);
      });
  }

}
