import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {PostService} from "../post.service";
import {AuthService} from "../../../shared/security/auth.service";
import {Router} from "@angular/router";
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styles: []
})
export class PostCreateComponent implements OnInit {

  public createForm: FormGroup;
  public files: any[] = [];
  public urls: any[] = [];

  public formErrors = {
    title: '',
    tegs: '',
    body: ''
  };

  private errorMessages = {
    title: {
      required: 'Заполните это поле',
    },
    tegs: {
      required: 'Заполните это поле',
      pattern: 'Можно использовать символы - _ буквы русского и английского алфавита и цифры'
    },
    body: {
      required: 'Заполните это поле',
    }
  };

  public process: boolean = false;
  public error: string;
  private maxFiles: number = 9;
  private images: any[] = [];

  constructor(private fb: FormBuilder,
              public toastr: ToastsManager,
              vcr: ViewContainerRef,
              private postService: PostService,
              private authService: AuthService,
              private router: Router)
  {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.createForm = this.fb.group({
      title: ['', Validators.required],
      tegs: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^([а-яёa-z0-9_-]+\s?)+$/i)
      ])],
      body: ['', Validators.required]
    });
    this.createForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.createForm) { return; }
    const form = this.createForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '. ';
        }
      }
    }
  }

  onChangeImage(event: any) {
    let fileList: FileList = event.target.files;
    if (!fileList.length) { return; }

    if (!this.checkFile(fileList[0])) {
      return false;
    }
    if (this.files.length > this.maxFiles) {
      this.error = 'К одной статьи можно добавить максимум 10 фото';
      return false;
    }

    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.error = null;
      this.urls.push({ url: event.target.result })
    };
    this.files.push(fileList[0]);
    reader.readAsDataURL(fileList[0]);
  }

  removeImage(id: number) {
    this.urls.splice(id, 1);
    this.files.splice(id, 1);
  }

  save() {
    this.error = null;
    if (!this.createForm.valid) { return; }

    this.process = true;
    const data = this.createForm.value;
    data.userId = this.authService.user['id'];
    this.postService.savePost(data).then(result => {
      const postId = Number(result);
      if (postId != 0 && this.files.length != 0) {

        let i = 0;
        for (const file of this.files) {
          this.postService.uploadFile(file, data.userId).then(res => {
            this.images.push(res);
            // Козда все изображения загружены сохраняем из названия в статью
            if (this.images.length === this.files.length) {
              this.postService.updatePostImages(postId, this.images).then(res => {
                this.process = false;
                console.log(res);
              });
            }
          }, error => {
            this.error = error.mesage;
            this.process = false;
          });
          i++;
        }
      }
      this.process = false;
      this.toastr.success('Пост успешно добавлен');
      this.resetForm();
      this.router.navigate(['/post/my']);
    }, error => {
      this.process = false;
      this.error = error.mesage;
    });
  }

  checkFile(file: any) {
    const blackList = [".php", ".phtml", ".php3", ".php4", ".html", ".htm"];
    for (const item of blackList) {
      if (file.name.indexOf(item) !== -1) {
        this.error = 'Так делать не хорошо, пожалуйста не взламывайте нас =)';
        return false;
      }
    }
    if (file.size < 102400) {
      this.error = 'Размер файла должен быть не менее 100 кб';
      return false;
    }
    if (file.size > 10240000) {
      this.error = 'Размер файла должен быть не более 10 мб';
      return false;
    }
    return true;
  }

  resetForm() {
    this.createForm.reset();
    this.files = [];
    this.images = [];
  }

}
