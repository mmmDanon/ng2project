import { Routes, RouterModule } from "@angular/router";
import { PostListComponent } from './post-list/post-list.component';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { UserPostsComponent } from './user-posts/user-posts.component';
import {AuthGuard} from "../../shared/security/auth.guard";

const postRoutes: Routes = [
  { path: '' , component: PostListComponent },
  { path: 'detail/:id', component: PostDetailComponent },
  { path: 'create', component: PostCreateComponent, canActivate: [AuthGuard] },
  { path: 'my', component: UserPostsComponent, canActivate: [AuthGuard] },
];

export const postRouting = RouterModule.forChild(postRoutes);
