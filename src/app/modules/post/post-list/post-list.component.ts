import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {PostService} from "../post.service";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import 'rxjs/add/operator/map';
import {PagerService} from "./pager.service";
import {AuthService} from "../../../shared/security/auth.service";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styles: [
    '.pagination a {cursor: pointer; padding: 5px 8px;}',
    '.comment-vote a {cursor: pointer;}'
  ]
})
export class PostListComponent implements OnInit {

  public postData: any[];
  public pager: any = {};
  private currentPage: number;
  private pagerInfo: any;

  constructor(private aRouter: ActivatedRoute,
              private postService: PostService,
              private pagerService: PagerService,
              private router: Router,
              private authService: AuthService,
              public toastr: ToastsManager,
              vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
    aRouter.queryParams.subscribe(params => {
      if (!params['page']) {
        this.currentPage = 1;
      } else {
        this.currentPage = params['page'];
      }
    });
  }

  ngOnInit() {
    this.postService.getPostList(this.currentPage).map(res => res.json())
      .subscribe(res => {
        this.postData = res.items;
        this.pagerInfo = res._meta;
        this.setPage(null);
      });
  }

  vote(vote: number, post: any) {
    const userId = this.authService.user['id'];
    const postId = post.id;
    this.postService.postVote(vote, postId, userId).map(res => res.json())
      .subscribe(res => {
        console.log(res);
        post.rating = res;
      }, error => {
        error = error.json();
        this.showError(error.message);
      });
  }
  showError(error) {
    this.toastr.error(error);
  }
  setPage(page?: number) {
    if (page != null) {
      if (page < 1 || page > this.pager.totalPages) {
        return;
      }
      this.currentPage = page;
      this.postService.getPostList(this.currentPage).map(res => res.json())
        .subscribe(res => {
          this.postData = res.items;
          this.pagerInfo = res._meta;
          this.pager = this.pagerService.getPager(this.pagerInfo.pageCount, page);
        });
    } else {
      page = this.currentPage;
      this.pager = this.pagerService.getPager(this.pagerInfo.pageCount, page);
    }
    this.router.navigate(['/post'], {queryParams:{page: page}});
  }

}
