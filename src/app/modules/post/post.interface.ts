export interface Post {

  id?: number;
  user_id?: number;
  title: string;
  body: string;
  status?: number;
  rating?: number;
  images?: string;
  created_at?: number;
  updated_at?: number;
  tags?: any;
  comments?: any;
}
