import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { postRouting } from './post.routing';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {PagerService} from "./post-list/pager.service";
import { UserPostsComponent } from './user-posts/user-posts.component';

@NgModule({
  imports: [
    CommonModule, postRouting, ReactiveFormsModule, FormsModule
  ],
  declarations: [PostDetailComponent, PostListComponent, PostCreateComponent, UserPostsComponent],
  providers: [PagerService]
})
export class PostModule { }
