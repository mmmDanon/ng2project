import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {PostService} from "../post.service";
import {AuthService} from "../../../shared/security/auth.service";
import {Router} from "@angular/router";
import 'rxjs/add/operator/map';
import {ToastsManager} from "ng2-toastr";

@Component({
  selector: 'app-user-posts',
  templateUrl: './user-posts.component.html',
  styleUrls: ['./user-posts.component.css']
})
export class UserPostsComponent implements OnInit {

  public posts: any[] = [];
  public user: any;

  constructor(private postService: PostService, private authService: AuthService, private router: Router,public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.user = this.authService.user;
    this.postService.getUserPostList(this.user.id).map(res => res.json())
      .subscribe(res => {
        this.posts = res;
      }, error => {
        error = error.json();
        this.toastr.error(error.message);
      });
  }

}
