import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {AuthHttp} from "../../shared/security/AuthHttp";
import {WebConfig} from "../../shared/web.config";


@Injectable()
export class PostService {
  private baseUrl: string;
  private addPostUrl: string = '/post/default/';
  private viewPostUrl: string = '/post/main/';

  constructor(private http: AuthHttp, private webConfig: WebConfig) {
    this.baseUrl = this.webConfig.baseUrl;
  }

  // Сохранение нового поста
  savePost(post: any) {
    return this.http.post(this.baseUrl + this.addPostUrl + 'create', post).toPromise()
      .then(res => res.json());
  }

  uploadFile(file: any, id: number) {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('postId', id);
    return this.http.post(this.baseUrl + this.addPostUrl+'upload', formData).toPromise()
      .then(res => res.json());
  }

  updatePostImages(id: number, images: any[])
  {
    return this.http.post(this.baseUrl + this.addPostUrl+'update-images', {id: id, images: images}).toPromise()
      .then(res => res.json());
  }

  // Просмотр поста
  getPostDetail(id: number) {
    return this.http.get(this.baseUrl + this.viewPostUrl + 'view?id='+id);
  }

  getPostList(page: number) {
    return this.http.get(this.baseUrl + this.viewPostUrl + 'index?page='+page);
  }

  saveComment(comment: any) {
    return this.http.post(this.baseUrl + this.viewPostUrl + 'save-comment', comment);
  }

  postVote(vote: number, postId: number,userId: number) {
    return this.http.post(this.baseUrl + this.viewPostUrl + 'post-vote', {user_vote: {user_id: userId, post_id: postId}, vote: vote});
  }

  getUserPostList(user_id: number) {
    return this.http.get(this.baseUrl + this.viewPostUrl + 'user-posts?user_id='+user_id)
  }








}
