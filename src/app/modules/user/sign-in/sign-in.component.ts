import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router, ActivatedRoute} from "@angular/router";

import 'rxjs/add/operator/map';

import {AuthService} from "../../../shared/security/auth.service";
import {UserService} from "../user.service";
import {ToastsManager} from "ng2-toastr";


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styles: []
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  public pending: boolean;
  public message: string = '';

  public formErrors = {
    username: '',
    password: ''
  };

  private errorMessages = {
    username: {
      required: 'Заполните это поле',
    },
    password: {
      required: 'Заполните это поле',
    }
  };

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router,
              private userService: UserService,
              private aRouter: ActivatedRoute,
              public toastr: ToastsManager, vcr: ViewContainerRef
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }


  ngOnInit() {
    this.aRouter.queryParams.subscribe(params => {
      this.message = params['message'];
      if (this.message) {
        this.showSuccess(this.message);
      }
    });
    this.pending = false;
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
    this.loginForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }
  showSuccess(msg) {
    this.toastr.success(msg);
  }
  onValueChanged(data?: any) {
    if (!this.loginForm) { return; }
    const form = this.loginForm;

    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '. ';
        }
      }
    }
  }

  login() {
    this.pending = true;
    this.userService.login(this.loginForm.value)
      .map(res => res.json())
      .subscribe(res => {
        this.authService.authenticate(res);
        this.pending = false;
        this.router.navigate(['/home']);
      }, error => {
        if (error.status === 422) {
          this.formErrors.password = 'Неверный логин или пароль';
          this.pending = false;
        }
      });
  }

}
