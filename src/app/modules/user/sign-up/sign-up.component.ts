import { Component, OnInit } from '@angular/core';
import {FormGroup, Validators, FormBuilder, FormControl} from "@angular/forms";

import 'rxjs/add/operator/map';

import {UserService} from "../user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styles: []
})
export class SignUpComponent implements OnInit {
  signupForm: FormGroup;
  public pending: boolean;

  public formErrors = {
    username: '',
    email: '',
    password: '',
    confirmPassword: ''
  };

  private errorMessages = {
    username: {
      required: 'Заполните это поле',
      minlength: 'Введите не мение 3-х символов',
      maxlength: 'Имя слишком длинное',
      duplicate: 'Такой email уже существует',
      pattern: 'Имя может содержать символы [a-z0-9_]'
    },
    email: {
      required: 'Заполните это поле',
      pattern: 'Некорректный email',
      duplicate: 'Такой email уже существует'
    },
    password: {
      required: 'Заполните это поле',
      minlength: 'Введите не мение 6-ти символов',
    },
    confirmPassword: {
      required: 'Заполните это поле',
      passwordsNotMatch: 'Пароли не совпадают'
    }
  };
  constructor(
    private userService: UserService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.pending = false;
    this.signupForm = this.fb.group({
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.pattern(/^[a-z]+[a-z0-9_]*$/)
      ])],
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.pattern(/^[a-zA-Z0-9_-]+$/)
      ])],
      confirmPassword: ['', Validators.compose([
        Validators.required,
        this.isEqualPassword.bind(this)
      ])]
    });
    this.signupForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  signUp() {
    this.pending = true;
    this.userService.signup(this.signupForm.value)
      .map(res => res.json())
      .subscribe(res => {
        this.pending = false;
        this.router.navigate(['/user'], {queryParams: {message: 'Регистрация прошла успешно, введите свои данные дял входа на сайт'}});
      }, error => {
        if (error.status === 422) {
          const errors = error.json();
          for (const key in errors) {
            this.formErrors[errors[key].field] = errors[key].message;
          }
        }
        this.pending = true;
      });
  }

  onValueChanged(data?: any) {
    if (!this.signupForm) { return; }
    const form = this.signupForm;

    for (const field in this.formErrors) {
      //  clear previous error
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '. ';
        }
      }
    }
  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.signupForm) {
      return {passwordsNotMatch: true};

    }
    if (control.value !== this.signupForm.controls['password'].value) {
      return {passwordsNotMatch: true};
    }
  }

}
