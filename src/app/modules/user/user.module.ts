import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import {ReactiveFormsModule} from "@angular/forms";
import {userRouting} from "./user.routing";
import {UserService} from "./user.service";
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
  imports: [
    CommonModule, ReactiveFormsModule, userRouting
  ],
  declarations: [SignInComponent, SignUpComponent, ForgotPasswordComponent, ProfileComponent],
  providers: [UserService]
})
export class UserModule { }
