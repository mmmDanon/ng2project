import {Routes, RouterModule} from "@angular/router";
import { SignInComponent } from './sign-in/sign-in.component'
import { SignUpComponent } from './sign-up/sign-up.component'
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component'
import {GuestGuard} from "../../shared/security/guest.guard";
import {ProfileComponent} from "./profile/profile.component";
import {AuthGuard} from "../../shared/security/auth.guard";

const userRoutes: Routes = [
    { path: '', component: SignInComponent, canActivate: [GuestGuard] },
    { path: 'sign-up', component: SignUpComponent, canActivate: [GuestGuard] },
    { path: 'forgot-password', component: ForgotPasswordComponent },
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] }
];

export const userRouting = RouterModule.forChild(userRoutes);
