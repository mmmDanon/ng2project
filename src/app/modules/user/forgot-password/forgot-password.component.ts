import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormControl} from "@angular/forms";
import {UserService} from "../user.service";
import 'rxjs/add/operator/map';
import {ToastsManager} from "ng2-toastr";
import {Router} from "@angular/router";
import {AuthService} from "../../../shared/security/auth.service";

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public stage: number = 1;
  public pending: boolean = false;
  public userInfo;

  resetForm: FormGroup;
  resetFormTwo: FormGroup;
  public formErrors = {
    email: ''
  };
  public formTwoErrors = {
    password: '',
    confirm_password: '',
    token: ''
  };

  private errorMessages = {
    email: {
      required: 'Заполните это поле',
      pattern: 'Некорректный email',
      noFind: 'Пользователь с таким email не найден'
    }
  };

  private errorTwoMessages = {
    token: {
      required: 'Заполните это поле'
    },
    password: {
      required: 'Заполните это поле',
      minlength: 'Введите не мение 6-ти символов',
    },
    confirm_password: {
      required: 'Заполните это поле',
      passwordsNotMatch: 'Пароли не совпадают'
    }
  };
  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private userService: UserService,
              public toastr: ToastsManager,
              vcr: ViewContainerRef,
              private router: Router) {
    this.toastr.setRootViewContainerRef(vcr);
    this.userInfo = this.authService.userInfo;
  }

  ngOnInit() {
    this.resetForm = this.fb.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)
      ])]
    });
    this.resetForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
    this.resetFormTwo = this.fb.group({
      token: ['', Validators.required],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])],
      confirm_password: ['', Validators.compose([
        Validators.required,
        this.isEqualPassword.bind(this)
      ])],
    });
    this.resetFormTwo.valueChanges.subscribe(data => this.onValueChangedTwo(data));
    this.onValueChangedTwo();
  }
  onValueChanged(data?: any) {
    if (!this.resetForm) {
      return;
    }
    const form = this.resetForm;

    for (const field in this.formErrors) {
      //  clear previous error
      this.formErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + '. ';
        }
      }
    }
  }
  onValueChangedTwo(data?: any) {
    if (!this.resetFormTwo) {
      return;
    }
    const form = this.resetFormTwo;

    for (const field in this.formTwoErrors) {
      //  clear previous error
      this.formTwoErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.errorTwoMessages[field];
        for (const key in control.errors) {
          this.formTwoErrors[field] += messages[key] + '. ';
        }
      }
    }
  }

  stageOne() {
    this.pending = true;
    if (!this.resetForm.valid) {
      return;
    }
    this.userService.generateResetToken(this.resetForm.value).map(res => res.json())
      .subscribe(res => {
        this.pending = false;
        this.stage = 2;
        this.showSuccess(res);
      }, error => {
        error = error.json();
        this.pending = false;
        this.formErrors.email = error.message;
        this.showError(error.message);
      });
  }

  stageTwo() {
    this.pending = true;
    if (!this.resetFormTwo.valid) {
      return;
    }
    const token = this.resetFormTwo.get('token').value;
    const password = this.resetFormTwo.get('password').value;

    this.userService.resetPassword({token: token, data: {password: password}}).map(res => res.json())
      .subscribe(res => {
        this.pending = false;
        this.stage = 1;
        this.router.navigate(['/user'], {queryParams: {message: res}});
      }, error => {
        error = error.json();
        this.pending = false;
        this.formErrors.email = error.message;
        this.showError(error.message);
      });
  }

  isEqualPassword(control: FormControl): {[s: string]: boolean} {
    if (!this.resetFormTwo) {
      return {passwordsNotMatch: true};

    }
    if (control.value !== this.resetFormTwo.controls['password'].value) {
      return {passwordsNotMatch: true};
    }
  }
  showSuccess(mes: string) {
    this.toastr.success(mes);
  }

  showError(mes: string) {
    this.toastr.error(mes);
  }

}
