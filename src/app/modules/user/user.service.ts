import { Injectable } from '@angular/core';
import {AuthHttp} from "../../shared/security/AuthHttp";
import {UserInterface} from "../../shared/security/user.interface";
import {WebConfig} from "../../shared/web.config";


@Injectable()
export class UserService {

  private baseUrl: string;
  private userUrl = '/user/default/';

  constructor(private http: AuthHttp, private webConfig: WebConfig) {
    this.baseUrl = this.webConfig.baseUrl;
  }

  login(user: UserInterface) {
    return this.http.post(this.baseUrl + this.userUrl + 'login', user);
  }

  signup(user: UserInterface) {
    return this.http.post(this.baseUrl + this.userUrl + 'signup', user);
  }
  generateResetToken(email: any) {
    return this.http.post(this.baseUrl + this.userUrl + 'password-reset-request', email);
  }
  resetPassword(data: any) {
    return this.http.post(this.baseUrl + this.userUrl + 'password-reset', data);
  }

}
