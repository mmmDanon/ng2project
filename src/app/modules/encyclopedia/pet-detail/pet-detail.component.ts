import {Component, OnInit} from '@angular/core';
import {Pet} from "../pet.interface";
import {EncyclopediaService} from "../encyclopedia.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-pet-detail',
  templateUrl: './pet-detail.component.html',
  styles: []
})
export class PetDetailComponent implements OnInit {

  public petId: number;
  public petDetail: Pet;
  public breadCrumb: any;

  public lightBox = {
    open: false,
    currentImage: null,
    petDetail: null
  };

  constructor(private es: EncyclopediaService, private aRouter: ActivatedRoute) {
    aRouter.params.subscribe(params => {
      this.petId = params['id'];
    });
  }

  ngOnInit() {
    this.es.getPetDetail(this.petId).then(res => {
      this.petDetail = res;
      this.breadCrumb = this.es.breadCrumbs[this.petDetail.type];
    });
  }

  public openLightBox(imageId: number) {
    this.lightBox.open = true;
    this.lightBox.currentImage = imageId;
    this.lightBox.petDetail = this.petDetail;
  }

}
