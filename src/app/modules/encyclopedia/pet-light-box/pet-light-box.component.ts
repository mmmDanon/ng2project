import {Component, Input} from '@angular/core';

@Component({
  selector: 'pet-light-box',
  templateUrl: './pet-light-box.component.html',
  styles: []
})
export class PetLightBoxComponent {

  @Input() lightBox: any;

  constructor() { }

  back() {
    if (this.lightBox.currentImage >= 1) {
      --this.lightBox.currentImage;
    }
  }
  next() {
    if (this.lightBox.currentImage < this.lightBox.petDetail.images.length - 1) {
      ++this.lightBox.currentImage;
    }
  }

  isFirst() {
    return this.lightBox.currentImage === 0
  }
  isLast() {
    return this.lightBox.currentImage === this.lightBox.petDetail.images.length - 1;
  }

}
