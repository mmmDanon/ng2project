import {Directive, HostListener, Input, ElementRef} from '@angular/core';

@Directive({
  selector: '[lightBoxClose]'
})
export class LightBoxCloseDirective {

  @Input() lightBox: any;

  @HostListener('mousedown', ['$event']) close($event) {
    if(this._eref.nativeElement.contains($event.target)) {
      this.lightBox.open = false;
    }
  }
  constructor(private _eref: ElementRef) { }

}
