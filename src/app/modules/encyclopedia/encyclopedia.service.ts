import { Injectable } from '@angular/core';
import {AuthHttp} from "../../shared/security/AuthHttp";
import {Pet} from "./pet.interface";
import {WebConfig} from "../../shared/web.config";

@Injectable()
export class EncyclopediaService {
  private baseUrl: string;
  public headerData: any;
  public petListData: any[] = [];
  public petDetailData: any[] = [];
  public breadCrumbs: any= {
    dogs: 'Собаки',
    cats: 'Кошки',
    fish: 'Рыбы',
    rodents: 'Грызуны',
    birds: 'Птицы',
    reptiles: 'Рептилии',
    vermin: 'Хорьки',
    snails: 'Улитки',
    insects: 'Насекомые и членистоногие',
  };

  private url: string = '/encyclopedia/default/';

  constructor(private http: AuthHttp, private webConfig: WebConfig) {
    this.baseUrl = this.webConfig.baseUrl;
  }

  getHeaderData() {
    if (!this.headerData) {
      return this.http.get(this.baseUrl + this.url + 'get-header-data').toPromise()
        .then(res => res.json())
        .then(res => this.headerData = res)
    }
    return Promise.resolve(this.headerData);
  }

  getPetList(type: string): Promise<Pet[]> {
    if (!this.petListData[type]) {
      return this.http.get(this.baseUrl + this.url + 'all?type='+type).toPromise()
        .then(res => res.json())
        .then(res => this.petListData[type] = res);
    }
    return Promise.resolve(this.petListData[type]);
  }

  getPetDetail(id: number) {
    if (!this.petDetailData[id]) {
      return this.http.get(this.baseUrl + this.url + 'detail?id='+id).toPromise()
        .then(res => res.json())
        .then(res => this.petDetailData[id] = res);
    }
    return Promise.resolve(this.petDetailData[id]);
  }




}
