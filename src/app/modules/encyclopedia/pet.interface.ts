export interface Pet
{
  id: number;
  type: string;
  subtype: string;
  name: string;
  description?: string;
  img?: string;
  images?: string;
  text?: string;
}
