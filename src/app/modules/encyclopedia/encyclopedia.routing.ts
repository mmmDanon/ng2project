import {Routes, RouterModule} from "@angular/router";
import { EncyclopediaComponent } from './encyclopedia/encyclopedia.component'
import { PetListComponent } from './pet-list/pet-list.component'
import { PetDetailComponent } from './pet-detail/pet-detail.component'

const encyclopediaRoutes: Routes = [
  { path: '', component: EncyclopediaComponent },
  { path: 'pet-list/:type', component: PetListComponent },
  { path: 'pet-detail/:id', component: PetDetailComponent },
];

export const encyclopediaRouting = RouterModule.forChild(encyclopediaRoutes);
