import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EncyclopediaComponent } from './encyclopedia/encyclopedia.component';
import { PetListComponent } from './pet-list/pet-list.component';
import { PetDetailComponent } from './pet-detail/pet-detail.component';
import {EncyclopediaService} from "./encyclopedia.service";
import {encyclopediaRouting} from "./encyclopedia.routing";
import {FormsModule} from "@angular/forms";
import {PetFilterPipe} from "./pet-list/pet-filter.pipe";
import { PetLightBoxComponent } from './pet-light-box/pet-light-box.component';
import {LightBoxCloseDirective} from "./pet-light-box/light-box-close.directive";

@NgModule({
  imports: [
    CommonModule,
    encyclopediaRouting,
    FormsModule,
  ],
  declarations: [EncyclopediaComponent, PetListComponent, PetDetailComponent, PetFilterPipe, PetLightBoxComponent, LightBoxCloseDirective],
  providers: [],
})
export class EncyclopediaModule { }
