import {Pipe, Injectable, PipeTransform} from "@angular/core";
@Pipe({
  name: 'petFilter',
  pure: false
})
@Injectable()
export class PetFilterPipe implements PipeTransform {
  transform(items: any, args?: any): any {
    const subtype = args[1];
    const searchText = args[0];

    if (subtype != 'all') {
      return items.filter(item => item.subtype === subtype);
    }
    if (!searchText) { return items }
    return items.filter(item => item.name.toLowerCase().indexOf(searchText.toLowerCase()) !== -1);
  }
}
