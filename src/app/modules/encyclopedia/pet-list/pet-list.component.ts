import { Component, OnInit } from '@angular/core';
import {EncyclopediaService} from "../encyclopedia.service";
import 'rxjs/add/operator/map';
import {ActivatedRoute} from "@angular/router";
import {Pet} from "../pet.interface";

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styles: [
    'a {cursor: pointer}'
  ]
})
export class PetListComponent implements OnInit {

  public headerData: any;
  public currentHeaderData: any;
  public currentType: string;
  public currentSubtype: string = 'all';
  public petList: Pet[];
  public searchText: string;
  public searchTextModel: string;

  constructor(private es: EncyclopediaService, private aRouter: ActivatedRoute) {
    aRouter.params.subscribe(param => {
      this.currentType = param['type'];
    });
  }

  ngOnInit() {
    this.es.getHeaderData().then(res => {
      this.headerData = res;
      this.setCurrentHeaderData();
    });
    this.es.getPetList(this.currentType).then(res => {
      this.petList = res;
    });
  }

  // Header
  setCurrentHeaderData() {
    this.currentHeaderData = this.headerData[this.currentType];
  }

  set subtype(subtype: string) {
    this.searchTextModel = null;
    this.searchText = null;
    this.currentSubtype = subtype;
  }

  get subtype(): string {
    return this.currentSubtype;
  }

  search() {
    this.currentSubtype = 'all';
    this.searchText = this.searchTextModel;
  }

  resetSearch() {
    this.searchTextModel = null;
    this.searchText = null;
    this.subtype = 'all';
  }

}
