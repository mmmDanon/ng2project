/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { EncyclopediaService } from './encyclopedia.service';

describe('EncyclopediaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EncyclopediaService]
    });
  });

  it('should ...', inject([EncyclopediaService], (service: EncyclopediaService) => {
    expect(service).toBeTruthy();
  }));
});
