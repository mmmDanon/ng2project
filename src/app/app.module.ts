import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
import {LocationStrategy, HashLocationStrategy} from "@angular/common";
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { AppComponent } from './app.component';

import {CoreModule} from "./core/core.module";
import {appRouting} from "./app.routing";
import {AuthService} from "./shared/security/auth.service";
import {AuthGuard} from "./shared/security/auth.guard";
import {GuestGuard} from "./shared/security/guest.guard";
import {AuthHttp} from "./shared/security/AuthHttp";
import {EncyclopediaService} from "./modules/encyclopedia/encyclopedia.service";
import {PostService} from "./modules/post/post.service";
import {WebConfig} from "./shared/web.config";


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CoreModule,
    appRouting,
    ToastModule.forRoot()
  ],
  providers: [
    Location, {provide: LocationStrategy, useClass: HashLocationStrategy},
    AuthService,
    AuthHttp,
    AuthGuard,
    GuestGuard,
    WebConfig,
    EncyclopediaService, PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
